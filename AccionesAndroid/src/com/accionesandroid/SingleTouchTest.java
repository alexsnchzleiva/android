package com.accionesandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class SingleTouchTest extends Activity implements OnTouchListener{
	
	StringBuilder builder = new StringBuilder();
	TextView textView;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.textView.setText("Toca y Arastra!!!");
		this.textView.setOnTouchListener(this);
		this.setContentView(this.textView);
	}

	public boolean onTouch(View v, MotionEvent event) {
		this.builder.setLength(0);
		
		switch(event.getAction()){
			case MotionEvent.ACTION_DOWN:
				this.builder.append("Down, ");
				break;
			case MotionEvent.ACTION_MOVE:
				this.builder.append("Move, ");
				break;
			case MotionEvent.ACTION_CANCEL:
				this.builder.append("Cancel, ");
				break;
			case MotionEvent.ACTION_UP:
				this.builder.append("Up, ");
				break;
		}
		
		this.builder.append(event.getX());
		this.builder.append(", ");
		this.builder.append(event.getY());
		
		String text = builder.toString();
		
		Log.d("TouchTest", text);
		this.textView.setText(text);
		
		return true;
	}

}
