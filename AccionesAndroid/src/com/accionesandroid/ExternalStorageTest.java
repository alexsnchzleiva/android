package com.accionesandroid;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

public class ExternalStorageTest extends Activity {
	
	TextView textView;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.setContentView(this.textView);
		
		String estado = Environment.getExternalStorageState();
		
		if(!estado.equals(Environment.MEDIA_MOUNTED)){
			this.textView.setText("No hay almacenamiento externo montado");
		}
		else{
			this.textView.setText("Si hay almacenamiento externo.");
			File externalDir = Environment.getExternalStorageDirectory();	
			File textFile = new File(externalDir.getAbsolutePath() + File.separator + 
					"texto.txt");
			try {
				this.writeTextFile(textFile, "Esto es una prueba del funcionamiento de la SD");
				String texto = this.readTextFile(textFile);
				this.textView.setText(texto);
				if(!textFile.delete()){
					this.textView.setText("No se ha podico eliminar el archivo temporal.");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	
			
		}
		
	}

	private String readTextFile(File textFile) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(textFile));
		StringBuilder texto = new StringBuilder ();
		String line;
		
		while(	(line = reader.readLine()) != null	){
			texto.append(line);
			texto.append("\n");
		}
		
		//reader.close();
		
		return texto.toString();
	}

	private void writeTextFile(File file, String string) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(string);
		writer.close();
	}

}
