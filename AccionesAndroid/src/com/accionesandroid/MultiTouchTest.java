package com.accionesandroid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class MultiTouchTest extends Activity implements OnTouchListener {

	StringBuilder builder;
	TextView textView;
	float [] x = new float[10];
	float [] y = new float[10];
	boolean [] tocado = new boolean[10];
	
	private void updateTextView(){
		this.builder.setLength(0);
		
		for(int i=0;i<10;i++){
			this.builder.append(this.tocado[i]);
			this.builder.append(", ");
			this.builder.append(this.x[i]);
			this.builder.append(", ");
			this.builder.append(this.y[i]);
			this.builder.append("\n");
		}
		this.textView.setText(this.builder.toString());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.textView.setText("Toca y arrastra!!");
		this.setContentView(this.textView);
	}
	
	public boolean onTouch(View arg0, MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
		int pointerId = event.getPointerId(pointerIndex);
		
		switch(action){
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				this.tocado[pointerId] = true;
				x[pointerId] = (int)event.getX(pointerIndex);
				y[pointerId] = (int)event.getY(pointerIndex);
				break;
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
				this.tocado[pointerId] = false;
				x[pointerId] = (int)event.getX(pointerIndex);
				y[pointerId] = (int)event.getY(pointerIndex);
				break;
			case MotionEvent.ACTION_MOVE:
				int pointerCount = event.getPointerCount();
				
				for(int i=0;i<pointerCount;i++){
					pointerIndex = i;
					pointerId = event.getPointerId(pointerIndex);
					x[pointerId] = (int)event.getX(pointerIndex);
					y[pointerId] = (int)event.getY(pointerIndex);
				}
				
				break;
		}
		
		this.updateTextView();
		
		return true;
	}

}
