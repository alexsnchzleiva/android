package com.accionesandroid;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity{
	
	String [] pruebas = {"LifeCycleTest","SingleTouchTest","MultiTouchTest","KeyTest",
			"AccelerometerTest","AssetsTest","ExternalStorageTest","SoundPoolTest"};
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		ArrayAdapter <String> adapter = new ArrayAdapter<String>(this, 
				android.R.layout.simple_list_item_1,pruebas);
		this.setListAdapter(adapter);
	}
	
	@Override
	protected void onListItemClick(ListView list, View view, int position, long id){
		super.onListItemClick(list, view, position, id);
		String nombrePrueba = this.pruebas[position];
		
		try {
			Class<?> clazz = Class.forName("com.accionesandroid." + nombrePrueba);
			Intent intent = new Intent(this, clazz);
			this.startActivity(intent);
		} 
		catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
}
