package com.accionesandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.TextView;

public class KeyTest extends Activity implements OnKeyListener {

	StringBuilder builder = new StringBuilder();
	TextView textView;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.textView.setText("Pulsa cualquier tecla!!!");
		this.textView.setOnKeyListener(this);
		this.textView.setFocusableInTouchMode(true);
		this.textView.requestFocus();
		this.setContentView(this.textView);
	}
	
	public boolean onKey(View arg0, int arg1, KeyEvent event) {
		this.builder.setLength(0);
		
		switch(event.getAction()){
			case KeyEvent.ACTION_DOWN:
				this.builder.append("Down, ");
				break;
			case KeyEvent.ACTION_UP:
				this.builder.append("Up, ");
				break;
		}
		
		this.builder.append(event.getKeyCode());
		this.builder.append(", ");
		this.builder.append((char)event.getUnicodeChar());
		String text = this.builder.toString();
		Log.d("KeyTest", text);
		this.textView.setText(text);
		
		if(event.getKeyCode() == KeyEvent.KEYCODE_BACK){
			return false;
		}
		else{
			return true;
		}
		
	}

}
