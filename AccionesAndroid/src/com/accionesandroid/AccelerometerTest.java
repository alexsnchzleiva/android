package com.accionesandroid;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;

public class AccelerometerTest extends Activity implements SensorEventListener {
	
	StringBuilder builder = new StringBuilder();
	TextView textView;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.setContentView(this.textView);
		
		SensorManager manager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
		
		if(manager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() == 0){
			this.textView.setText("No hay un acelerometro instalado");
		}
		else{
			Sensor acelerotemer = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
			
			if(manager.registerListener(this, acelerotemer,SensorManager.SENSOR_DELAY_GAME)){
				this.textView.setText("No se ha podido registrar el SensorListener");
			}
				
		}
			
	}

	public void onAccuracyChanged(Sensor arg0, int arg1) {
	}

	public void onSensorChanged(SensorEvent event) {
		this.builder.setLength(0);
		this.builder.append("X: ");
		this.builder.append(event.values[0]);
		this.builder.append(", Y: ");
		this.builder.append(event.values[1]);
		this.builder.append(", Z: ");
		this.builder.append(event.values[2]);
		this.textView.setText(this.builder.toString());
	}

}
