package com.accionesandroid;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;

public class SoundPoolTest extends Activity implements OnTouchListener {
	
	TextView textView;
	SoundPool soundPool;
	int miSonidoId = -1;
	AssetManager assetManager;
	AssetFileDescriptor assetFileDescriptor; 
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.textView.setOnTouchListener(this);
		this.textView.setText("Pulsa para que suena!!");
		this.setContentView(this.textView);
		this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0);
		
		try {
			this.assetManager = this.getAssets();
			this.assetManager.openFd("rh.ogg");
			this.miSonidoId = this.soundPool.load(this.assetFileDescriptor, 1);
		} catch (IOException e) {
			e.printStackTrace();
			this.textView.setText("No se ha podido cargar el sonido");
		}
		
	}

	public boolean onTouch(View arg0, MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_UP){
			if(this.miSonidoId != -1){
				this.soundPool.play(this.miSonidoId, 1, 1, 0, 0, 1);
			}
			
		}
		
		return true;
	}

}
