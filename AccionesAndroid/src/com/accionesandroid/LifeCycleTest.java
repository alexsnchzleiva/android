package com.accionesandroid;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class LifeCycleTest extends Activity {
	
	StringBuilder builder = new StringBuilder();
	TextView textView;
	
	private void log(String texto){
		Log.d("LifeCycleTest", texto);
		builder.append(texto);
		builder.append("\n");
		textView.setText(builder.toString());
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.textView.setText(this.builder.toString());
		this.setContentView(this.textView);
		this.log("Created");
	}
	
	@Override
	protected void onResume(){
		super.onResume();
		this.log("Resume");
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		this.log("Pause");
		
		if(this.isFinishing()){
			this.log("Finishing");
		}
	}
	
	

}
