package com.accionesandroid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.TextView;

public class AssetsTest extends Activity {
	
	TextView textView;
	AssetManager assetManager;
	InputStream inputStream;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		this.textView = new TextView(this);
		this.setContentView(this.textView);
		
		this.assetManager = this.getAssets();
		this.inputStream = null;
		
		try {
			this.inputStream = assetManager.open("canciones/endOfTheWorld.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}
		String text;
		try {
			text = this.cargararchivos(this.inputStream);
			this.textView.setText(text);
		} catch (IOException e) {
			this.textView.setText("No se puede cargar el Archivo.");
			e.printStackTrace();
		}
		finally{
			if(this.inputStream != null){
				try {
					this.inputStream.close();
				} catch (IOException e) {
					this.textView.setText("No se puede cerrar el archivo.");
					e.printStackTrace();
				}
			}
		}
		
	}

	private String cargararchivos(InputStream inputStream2) throws IOException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byte [] bytes = new byte[4096];
		int len = 0;
		
		while((len = this.inputStream.read(bytes)) > 0){
			byteArrayOutputStream.write(bytes, 0, len);
		}
		
		return new String(byteArrayOutputStream.toByteArray(), "UTF8");
	}

}
